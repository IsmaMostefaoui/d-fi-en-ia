#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
from yolo import YOLO, detect_video
from PIL import Image


def detect_img(yolo, image_path, output_path=''):
    try:
        image = Image.open(image_path)
    except:
        print ('Open Error! Try again!')
    r_image = yolo.detect_image(image)
    r_image.save(output_path)

if __name__=='__main__':
    k=0
    yolo=YOLO('model_data/key_classes.txt','model_data/yolo_anchors.txt','weights_yolo_train/trained_weights_final.h5')
    for i in os.listdir("./keys_and_background/"):
        output_path="outputFolder/yolo"+str(k)+".jpg"
        k+=1
        input_path="keys_and_background/"+i
        detect_img(yolo, input_path, output_path)
        